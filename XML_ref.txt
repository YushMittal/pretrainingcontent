XML - eXtensible Markup Language

	Characterstics

	it was designed to store and transport data
	it is both human and machine readable.
	its tags aren't predefined but are case sensitive.

	Syntax

	<xml version="1.0" encoding="UTF-8"?>
	<root>

		<child>
	
			<subchild>

			</subchild>

		</child>
	</root>

Five pre-defined entity references

&lt; for less than <
&gt; for greater than >
&amp; for ampersand &
&ap; for apostrophe
&quot; for quote "

XMLHttpRequest object can be used to request data from a web server.

Advantages

Update a web page without reloading the page
Request data from a server - after the page has loaded
Receive data from a server  - after the page has loaded
Send data to a server - in the background

XML parser is used to access and manipulate XML, before accessing and XML it must be loaded into an XML DOM object.

	Example

	<html>
	<body>

	<p id="demo"></p>

	<script>
	var text, parser, xmlDoc;

	text = "<bookstore><book>" +
		"<title>Everyday Italian</title>" +
		"<author>Giada De Laurentiis</author>" +
		"<year>2005</year>" +
		"</book></bookstore>";

	parser = new DOMParser();
	xmlDoc = parser.parseFromString(text,"text/xml");

	document.getElementById("demo").innerHTML =
	xmlDoc.getElementsByTagName("title")[0].childNodes[0].nodeValue;
	</script>

	</body>
	</html>

